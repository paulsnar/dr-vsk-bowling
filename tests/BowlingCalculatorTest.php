<?php
namespace PN\Bowling\Tests;

use PHPUnit\Framework\TestCase;
use PN\Bowling\{Frame, BowlingCalculator};

class BowlingCalculatorTest extends TestCase
{
    /** @var BowlingCalculator */
    private $calculator;

    public function setUp()
    {
        parent::setUp();

        $this->calculator = new BowlingCalculator();
    }

    public function test_withNoHits_returnsScoreZero()
    {
        for ($i = 0; $i < 10; $i += 1) {
            $this->calculator->registerFrame(new Frame(0, 0));
        }
        $score = $this->calculator->getScore();
        $this->assertEquals(0, $score);
    }

    public function test_withSimpleThrows_calculatesScore()
    {
        for ($i = 0; $i < 10; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }
        $score = $this->calculator->getScore();
        $this->assertEquals(20, $score);
    }

    public function test_withSpareThrow_shouldAddMorePoints()
    {
        $this->calculator->registerFrame(new Frame(5, 5));
        $this->calculator->registerFrame(new Frame(3, 1));

        for ($i = 2; $i < 10; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }

        $score = $this->calculator->getScore();
        $this->assertEquals(33, $score);
    }

    public function test_withSpareThrowInLastFrame_doesNotAccessNonexistantFrames()
    {
        for ($i = 0; $i < 9; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }
        $this->calculator->registerFrame(new Frame(5, 5));

        $score = $this->calculator->getScore();
        $this->assertEquals(28, $score);
    }

    public function test_withStrikeThrow_shouldAddEvenMorePoints()
    {
        $this->calculator->registerFrame(new Frame(10));
        $this->calculator->registerFrame(new Frame(5, 2));

        for ($i = 2; $i < 10; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }

        $score = $this->calculator->getScore();
        $this->assertEquals(40, $score);
    }

    public function test_withPerfectGame_scoresThreeHundred()
    {
        // NOTE: this is an 11 because a strike in the 10th frame requires an
        // 11th frame to be played
        for ($i = 0; $i < 11; $i += 1) {
            $this->calculator->registerFrame(new Frame(10));
        }

        $score = $this->calculator->getScore();
        $this->assertEquals(300, $score);
    }

    public function test_whenRegisteringTooManyFrames_throws()
    {
        for ($i = 0; $i < 10; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }

        $this->expectException(\Throwable::class);
        $this->calculator->registerFrame(new Frame(1, 1));
    }

    public function test_whenRegisteringTooManyFramesWithFinalStrike_throws()
    {
        for ($i = 0; $i < 9; $i += 1) {
            $this->calculator->registerFrame(new Frame(1, 1));
        }
        $this->calculator->registerFrame(new Frame(10));
        $this->calculator->registerFrame(new Frame(1, 1));

        $this->expectException(\LogicException::class);
        $this->calculator->registerFrame(new Frame(1, 1));
    }

    public function test_whenRegisteringIncompleteFrame_throws()
    {
        $this->expectException(\LogicException::class);
        $this->calculator->registerFrame(new Frame(1));
    }
}
