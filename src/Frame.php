<?php


namespace PN\Bowling;


class Frame
{
    /** @var ?int */
    public $firstBall, $secondBall;

    public function __construct(?int $firstBall = null, ?int $secondBall = null)
    {
        $this->firstBall = $firstBall;
        $this->secondBall = $secondBall;
    }

    public function isStrike(): bool
    {
        return $this->firstBall === 10;
    }

    public function isSpare(): bool
    {
        return ! $this->isStrike() &&
            $this->firstBall + $this->secondBall === 10;
    }

    public function isFinished(): bool
    {
        return $this->isStrike() || $this->secondBall !== null;
    }
}
