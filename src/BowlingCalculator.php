<?php
namespace PN\Bowling;

class BowlingCalculator
{
    /** @var Frame[] */
    private $frames = [];

    public function registerFrame(Frame $f)
    {
        if ( ! $f->isFinished()) {
            throw new \LogicException("Tried to add incomplete frame");
        }

        $frameCount = count($this->frames);

        if ($frameCount === 10) {
            $lastFrame = $this->frames[9];
            if ( ! ($lastFrame->isSpare() || $lastFrame->isStrike())) {
                throw new \LogicException("Tried to add too many frames");
            }
        } else if ($frameCount === 11) {
            throw new \LogicException("Tried to add too many frames");
        }

        $this->frames[] = $f;
    }

    public function getScore(): int
    {
        /** @var int $score */
        $score = 0;

        foreach ($this->frames as $i => $frame) {
            $nextFrame = $this->frames[$i + 1] ?? null;

            if ($nextFrame !== null) {
                if ($frame->isStrike()) {
                    $score += $nextFrame->firstBall + $nextFrame->secondBall;
                    if ($nextFrame->isStrike()) {
                        $nextNextFrame = $this->frames[$i + 2] ?? null;
                        if ($nextNextFrame !== null) {
                            $score += $nextNextFrame->firstBall +
                                $nextNextFrame->secondBall;
                        }
                    }
                } else if ($frame->isSpare()) {
                    $score += $nextFrame->firstBall;
                }
            }
            $score += $frame->firstBall + $frame->secondBall;
        }

        return $score;
    }
}
